# new feature
# Tags: optional

Feature: Update values in Datatables

  Scenario: Update values in a row from Datatable with Inline Edit
    Given Pedro is searching the lightning web component "datatable" from the "Component Reference"
    When run the selected option "Data Table with Inline Edit" from the Example section
    And update the row number 3 with the following values
      | Label      | Website            | Phone          | CloseAt       | Balance |
      | Larry Page | https://google.com | (555)-755-6575 | Today-12:57PM | $770.54 |
    Then all the values should be updated