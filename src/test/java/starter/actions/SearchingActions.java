package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import starter.locators.SearchingLocators;

public class SearchingActions extends UIInteractionSteps {
    @Step
    public void lookForTheComponent(String componentToFind) {
        $(SearchingLocators.FROM_QUICK_FIND).sendKeys(componentToFind);
    }

    @Step
    public void selectTheComponentFromLigthningWebComponents(String componentToFind) {
        $(SearchingLocators.LIGTHNING_WEB_COMPONENTS).findElement(By.xpath(String.format(".//a[.='%s']", componentToFind))).click();
    }
}
