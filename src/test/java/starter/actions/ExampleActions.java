package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import starter.locators.ExampleFormLocators;

public class ExampleActions extends UIInteractionSteps {
    IFrameManagementActions iFrameManagement;

    @Step
    public void selectAndRunTheTypeOfDataTable(String optionToSelect) {
        iFrameManagement.waitForSpinnerInsideChildIFrameIsVisible();
        $(ExampleFormLocators.EXAMPLE_FIELD).click();
        $(ExampleFormLocators.EXAMPLE_OPTIONS).findElement(By.xpath(String.format(".//lightning-base-combobox-item[.//span[.='%s']]", optionToSelect))).click();
        $(ExampleFormLocators.RUN_BUTTON).click();
    }


}
