package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import starter.locators.DataTableInlineEditLocators;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataTableInlineEditActions extends UIInteractionSteps {
    private int rowDataToUpdate;
    IFrameManagementActions iFrameManagementActions;

    @Step
    public void fromDataTableWithInlineEdit(List<Map<String, String>> newData, int rowToUpdate) {
        rowDataToUpdate = rowToUpdate;
        iFrameManagementActions.changeToIFrameForInteractingWithPreviewTable();
        editData(newData);
    }

    @Step
    private void editData(List<Map<String, String>> newData) {
        editLabelValue(newData.get(0).get("Label"), "Label");
        editWebsiteValue(newData.get(0).get("Website"), "Website");
        editPhoneValue(newData.get(0).get("Phone"), "Phone");
        editCloseAtValue(newData.get(0).get("CloseAt"), "CloseAt");
        editBalanceValue(newData.get(0).get("Balance"), "Balance");
    }

    @Step
    private void editLabelValue(String value, String columnToSelect) {
        enterNewValue(value, columnToSelect);
    }

    @Step
    private void editWebsiteValue(String value, String columnToSelect) {
        enterNewValue(value, columnToSelect);
    }

    @Step
    private void editPhoneValue(String value, String columnToSelect) {
        enterNewValue(value, columnToSelect);
    }

    @Step
    private void editCloseAtValue(String closeAt, String columnToSelect) {
        getRowDataToUpdate().findElement(By.xpath(String.format("./*[@data-label='%s']//button", columnToSelect))).click();
        enterDatePicker(closeAt);
        enterTimePicker(closeAt);
    }

    private void enterDatePicker(String closeAt) {
        String datePicker = closeAt.split("-")[0];
        if(datePicker.equals("Today")) {
            $(DataTableInlineEditLocators.FORM_DATE_PICKER_DATA_TABLE).findElement(By.xpath(".//input")).click();
            $(DataTableInlineEditLocators.FORM_DATE_PICKER_DATA_TABLE).findElement(By.xpath(".//lightning-calendar//*[.='Today']")).click();
        } else {
            $(DataTableInlineEditLocators.FORM_DATE_PICKER_DATA_TABLE).findElement(By.xpath(".//input")).clear();
            $(DataTableInlineEditLocators.FORM_DATE_PICKER_DATA_TABLE).findElement(By.xpath(".//input")).sendKeys(datePicker);
        }
    }

    private void enterTimePicker(String closeAt) {
        String timePicker = closeAt.split("-")[1];
        $(DataTableInlineEditLocators.FORM_INPUT_TIME_PICKER_DATA_TABLE).click();
        $(DataTableInlineEditLocators.FORM_INPUT_TIME_PICKER_DATA_TABLE).clear();
        $(DataTableInlineEditLocators.FORM_INPUT_TIME_PICKER_DATA_TABLE).type(timePicker, Keys.TAB);
    }

    @Step
    private void editBalanceValue(String value, String columnToSelect) {
        enterNewValue(value, columnToSelect);
    }

    private void enterNewValue(String value, String columnToSelect) {
        getRowDataToUpdate().findElement(By.xpath(String.format("./*[@data-label='%s']//button", columnToSelect))).click();
        $(DataTableInlineEditLocators.FORM_GENERAL_INPUT_DATA_TABLE).clear();
        $(DataTableInlineEditLocators.FORM_GENERAL_INPUT_DATA_TABLE).type(value, Keys.TAB);
    }

    private WebElement getRowDataToUpdate() {
        waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//table[contains(@class,'slds-table')]")));
        return $(DataTableInlineEditLocators.DATA_TABLE_WITH_INLINE_EDIT).findElement(By.xpath(String.format(".//tr[%d]", rowDataToUpdate)));
    }

    public List<String> getDataFromDataTableWithInlineEdit(int rowDataToUpdate) {
        this.rowDataToUpdate = rowDataToUpdate;
        return getRowDataToUpdate().findElements(By.xpath(".//*[@data-label]//span/div"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }
}
