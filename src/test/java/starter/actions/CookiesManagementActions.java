package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import starter.locators.CookiesHandlerLocators;

public class CookiesManagementActions extends UIInteractionSteps {
    @Step
    public void closeCookies() {
        $(CookiesHandlerLocators.CLOSE_COOKIES).click();
    }
}
