package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import starter.pages.DeveloperSaleForceHomePage;
import starter.locators.MainContentLocators;

public class NavigateActions extends UIInteractionSteps {
    DeveloperSaleForceHomePage developerSaleForceHomePage;

    @Step
    public void navigateToTheDeveloperSaleForceHomePage() {
        developerSaleForceHomePage.open();
    }

    @Step
    public void navigateToTheComponentLibrary(String componentLibrary) {
        waitFor(ExpectedConditions.presenceOfElementLocated(MainContentLocators.EXTERNAL_DOCUMENTATION));
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", $(By.xpath(String.format("//a[.='%s']", componentLibrary))));
    }
}
