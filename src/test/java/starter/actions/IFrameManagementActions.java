package starter.actions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class IFrameManagementActions extends UIInteractionSteps {

    public void waitForSpinnerInsideChildIFrameIsVisible() {
        WebElement frame = $(By.xpath("//iframe[@name='preview']"));
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath("//iframe[@name='preview']")));
        getDriver().switchTo().frame(frame);
        waitFor(ExpectedConditions.visibilityOf($(By.xpath(".//playground-preview//lightning-spinner"))));
        getDriver().switchTo().parentFrame();
    }

    public void changeToIFrameForInteractingWithPreviewTable() {
        WebElement frame = $(By.xpath("//iframe[@name='preview']"));
        getDriver().switchTo().frame(frame);
        waitFor(ExpectedConditions.presenceOfElementLocated(By.xpath(".//iframe[@name='preview']")));
        getDriver().switchTo().frame(0);
    }
}
