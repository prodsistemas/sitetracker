package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.actions.*;
import starter.questions.DataTableQuestions;

import java.util.List;
import java.util.Map;

public class UpdateDatatableDefinitions {
    List<Map<String, String>> data;
    int rowToUpdate;
    @Steps
    NavigateActions navigationActions;
    @Steps
    CookiesManagementActions cookiesManagementActions;
    @Steps
    SearchingActions searchingActions;
    @Steps
    ExampleActions doingWithExample;
    @Steps
    DataTableInlineEditActions updatingSomeData;
    @Steps
    DataTableQuestions dataTableQuestions;

    @Given("Pedro is searching the lightning web component {string} from the {string}")
    public void is_searching_the_lightning_web_component_from_the(String component, String module) {
        navigationActions.navigateToTheDeveloperSaleForceHomePage();
        cookiesManagementActions.closeCookies();
        navigationActions.navigateToTheComponentLibrary(module);
        searchingActions.lookForTheComponent(component);
        searchingActions.selectTheComponentFromLigthningWebComponents(component);
    }

    @When("run the selected option {string} from the Example section")
    public void run_the_selected_the_option_from_the_section(String optionToSelect) {
        doingWithExample.selectAndRunTheTypeOfDataTable(optionToSelect);
    }

    @When("update the row number {int} with the following values")
    public void update_the_values(int rowToUpdate, DataTable newData) {
        this.rowToUpdate = rowToUpdate;
        this.data = newData.asMaps(String.class, String.class);
        updatingSomeData.fromDataTableWithInlineEdit(this.data, this.rowToUpdate);
    }

    @Then("all the values should be updated")
    public void all_the_values_should_be_updated() {
        dataTableQuestions.verifyAllDataHaveBeenUpdatedInTheTable(this.data.get(0), this.rowToUpdate);
    }
}
