package starter.locators;

import org.openqa.selenium.By;

public class SearchingLocators {
    public static By LIGTHNING_WEB_COMPONENTS = By.xpath("//div[contains(@class,'lwc-section')]");
    public static By FROM_QUICK_FIND = By.xpath("//input[@name='Quick Find']");
}
