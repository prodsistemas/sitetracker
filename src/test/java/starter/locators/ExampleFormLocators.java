package starter.locators;

import org.openqa.selenium.By;

public class ExampleFormLocators {
    public static By EXAMPLE_FIELD = By.xpath(".//input[@name='example']");
    public static By EXAMPLE_OPTIONS = By.xpath(".//div[contains(@class,'slds-listbox')]");
    public static By RUN_BUTTON = By.xpath(".//lightning-button-group[contains(@class,'slds-button-group')]//button");
}
