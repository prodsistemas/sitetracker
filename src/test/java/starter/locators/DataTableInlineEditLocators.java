package starter.locators;

import org.openqa.selenium.By;

public class DataTableInlineEditLocators {
    public static By DATA_TABLE_WITH_INLINE_EDIT = By.xpath("//table[contains(@class,'slds-table')]");
    public static By FORM_GENERAL_INPUT_DATA_TABLE = By.xpath("//lightning-primitive-datatable-iedit-panel//input");
    public static By FORM_DATE_PICKER_DATA_TABLE = By.xpath(".//lightning-datepicker");
    public static By FORM_INPUT_TIME_PICKER_DATA_TABLE = By.xpath(".//lightning-timepicker//input");
}
