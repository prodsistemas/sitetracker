package starter.questions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.hamcrest.MatcherAssert;
import starter.actions.DataTableInlineEditActions;

import java.util.List;
import java.util.Map;

public class DataTableQuestions extends UIInteractionSteps {
    DataTableInlineEditActions dataTableInlineEditActions;

    @Step
    public void verifyAllDataHaveBeenUpdatedInTheTable(Map<String, String> expectedData, int rowToUpdate) {
        List<String> actualData = dataTableInlineEditActions.getDataFromDataTableWithInlineEdit(rowToUpdate);
        MatcherAssert.assertThat("The Label value does not match", actualData.contains(expectedData.get("Label")));
        MatcherAssert.assertThat("The Website value does not match", actualData.contains(expectedData.get("Website")));
        MatcherAssert.assertThat("The Phone value does not match", actualData.contains(expectedData.get("Phone")));
        MatcherAssert.assertThat("The Balance value does not match", actualData.contains(expectedData.get("Balance")));

    }
}
