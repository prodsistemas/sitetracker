package starter.pages;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://developer.salesforce.com/docs/component-library/documentation/en/48.0/lwc")
public class DeveloperSaleForceHomePage extends PageObject {
}
